package com.harishl.thealarmapp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.work.*
import kotlinx.coroutines.*


class CustomReceiver : BroadcastReceiver() {

    private lateinit var notifManager: NotificationManager

    override fun onReceive(context: Context, intent: Intent) {
        openWorkManager(context)

        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        notifManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        sendNotification(context)
        deliverNotif(context)
        Log.d("CUSTOM RECEIVER", "onReceive called")


        /*val intent = Intent(context, NewAlarmActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)*/
    }

    private fun openWorkManager(context: Context) {
        val apx = context.applicationContext
        val workManager = WorkManager.getInstance(apx)
        workManager.enqueue(OneTimeWorkRequest.from(LaunchActivityWorker::class.java))
    }

    private fun sendNotification(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notifChannel = NotificationChannel(
                MainActivity.CHANNEL_ID,
                "dummy_notif",
                NotificationManager.IMPORTANCE_HIGH
            )
            notifChannel.apply {
                enableLights(true)
                lightColor = Color.RED
                enableVibration(true)
                description = "Notify the alarm when it rings"
            }
            notifManager.createNotificationChannel(notifChannel)
        }
    }

    private fun deliverNotif(context: Context) {
        val intent = Intent(context, MainActivity::class.java)
        val pending = PendingIntent.getActivity(
            context,
            MainActivity.NOTIFICATION_ID,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val builder = NotificationCompat.Builder(context, MainActivity.CHANNEL_ID)
        builder.apply {
            setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)
                .setContentTitle("Alarm bruh!")
                .setContentText("Let's play the song right now")
                .setContentIntent(pending)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
        }
        notifManager.notify(MainActivity.NOTIFICATION_ID, builder.build())
    }
}

class LaunchActivityWorker(val context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {

    override suspend fun doWork(): Result =
        coroutineScope {
            launch {
                Log.d("WorkManager", "Worker called")
                withContext(Dispatchers.Main) {
                    Toast.makeText(context, "The bell's been rung. Out in the dark. Among the stars...", Toast.LENGTH_LONG).show()
                }
                val intent = Intent(applicationContext, NewAlarmActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(applicationContext, intent, null)
                Result.success()
            }
            Result.success()
        }
}