package com.harishl.thealarmapp

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.LightingColorFilter
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.widget.TimePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.judemanutd.autostarter.AutoStartPermissionHelper
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val NOTIFICATION_ID = 0
        const val CHANNEL_ID = "channel_id"
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //AutoStartPermission for some OEMs
//        AutoStartPermissionHelper.getInstance().getAutoStartPermission(this)

        val notifyIntent = Intent(this, CustomReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            this,
            0,
            notifyIntent,
            0
        )

        val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager

        toggle_alarm.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                Toast.makeText(this, "Alarm Turned ON", Toast.LENGTH_SHORT).show()
                /*alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                         SystemClock.elapsedRealtime()+5000,120000, pendingIntent)
                */
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    alarmManager.setExactAndAllowWhileIdle(
                        AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() + 12000,
                        pendingIntent
                    )
                } else {
                    alarmManager.setExact(
                        AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 12000,
                        pendingIntent
                    )
                }
            } else {
                alarmManager.cancel(pendingIntent)
                Toast.makeText(this, "Alarm Cancelled", Toast.LENGTH_LONG).show()
            }
        }

    }


}